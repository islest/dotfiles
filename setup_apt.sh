#!/bin/sh

# For 'apt' repositories
BASEDIR=$(dirname "$0")

# This installs ansible, latest version
add-apt-repository ppa:ansible/ansible -y
apt-get update
apt-get install ansible -y

# This runs the playbook hopefully. If it doesn't work you can just run it yourself
ansible-playbook $BASEDIR/playbook/site.yml --connection=local
