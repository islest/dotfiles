Dotfiles
========

An ansible playbook for configuring my dotfiles the way I like them. Currently supports only systems that run the apt package manager.

## Usage:
```
# On brand a new system:
sudo ./setup_apt.sh

# If the ansible-playbook run doesn't work:
cd playbook
sudo ansible-playbook site.yml --connection=local
```

### Currently updates the following:

* Installs common packages
* Vim configs + plugins
* Bash configurations
* Some Tmux hacks
